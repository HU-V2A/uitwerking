package model;

import ninja.validation.Validation;

import javax.validation.constraints.Size;

/**
 * Created by IT-Apps on 16/12/2016.
 */
public class Form {

    @Size(min = 1)
    private String voornaam;
    @Size(min = 1)
    private String achternaam;
    @Size(min = 1)
    private String straat;
    @Size(min = 1)
    private String postcode;
    @Size(min = 1)
    private String woonplaats;
    @Size(min = 1)
    private String email;
    @Size(min = 1)
    private String telefoonnummer;

    public Form() {
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public String getVoornaam() { return voornaam; }

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getStraat() {
        return straat;
    }

    public void setStraat(String straat) {
        this.straat = straat;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWoonplaats() {
        return woonplaats;
    }

    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefoonnummer() {
        return telefoonnummer;
    }

    public void setTelefoonnummer(String telefoonnummer) {
        this.telefoonnummer = telefoonnummer;
    }

    @Override
    public String toString() {
        return "Form{" +
                "voornaam='" + voornaam + '\'' +
                ", achternaam='" + achternaam + '\'' +
                ", straat='" + straat + '\'' +
                ", postcode='" + postcode + '\'' +
                ", woonplaats='" + woonplaats + '\'' +
                ", email='" + email + '\'' +
                ", telefoonnummer='" + telefoonnummer + '\'' +
                '}';
    }
}
