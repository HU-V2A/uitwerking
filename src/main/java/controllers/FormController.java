package controllers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import model.Form;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import com.google.inject.Singleton;
import ninja.params.PathParam;
import ninja.postoffice.Mail;
import ninja.postoffice.Postoffice;
import ninja.session.FlashScope;
import ninja.validation.*;
import ninja.validation.Validation;
import org.apache.commons.mail.EmailException;
import javax.validation.*;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


/**
 * Created by IT-Apps on 16/12/2016.
 */

@Singleton
public class FormController {

    @Inject
    Provider<Mail> mailProvider;

    @Inject
    Postoffice postoffice;

    public Result postForm(@JSR303Validation Form form, Validation validation, FlashScope flashScope) {

        if(validation.hasViolations()) {
            flashScope.error("Vull alle velden in aub!");

            return Results.redirect("/form");
        } else{

            flashScope.success("Verzonden!");
        }

        sendMail(form);

        return Results.html().render(form);

    }

    private boolean sendMail(Form form) {
        final String username = "hu@it-apps.nl";
        final String password = "McFlurry@12";
        final String from = "hu@it-apps.nl";
        final String objet = "Onderwerp";
        final String html = form.toString();
        final String to = "Danny@it-apps.nl";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(objet);
            message.setText(html, "utf-8", "html");
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public Result index() {

        return Results.html();
    }
}


