package controllers;
/**
 * Created by pascal on 13-12-16.
 */

import ninja.Result;
import ninja.Results;
import com.google.inject.Singleton;


@Singleton
public class HomeController {

    public Result index() {
        return Results.html();
    }

}
